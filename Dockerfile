FROM gliderlabs/alpine:3.1

RUN apk update && apk add git go && rm -rf /var/cache/apk/*

ENV GOROOT /usr/lib/go
ENV GOPATH /go
ENV GOBIN /go/bin
ENV PATH $PATH:$GOROOT/bin:$GOPATH/bin

# copy the local package files to the container's workspace.
ADD . /go/src/bitbucket.org/fernandezpablo85/lagone

# build dependencies.
RUN go get github.com/tools/godep
RUN go get github.com/revel/cmd/revel
RUN cd /go/src/bitbucket.org/fernandezpablo85/lagone && godep restore

# run lagone on start.
CMD ["revel", "run", "bitbucket.org/fernandezpablo85/lagone", "prod"]

# the service listens on 9000 by default.
EXPOSE 8080
