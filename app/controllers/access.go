package controllers

import (
	m "bitbucket.org/fernandezpablo85/lagone/app/models"
	"github.com/revel/revel"
	"net/http"
	t "time"
)

type Access struct {
	LagoneApp
}

func isValid(user string, pass string) bool {
	return last(user) == last(pass)
}

func isValidPassword(pass string) bool {
	return len(pass) > 4
}

func last(s string) string {
	return s[len(s)-1 : len(s)]
}

func (c Access) Login() revel.Result {
	// simulate lantency.
	t.Sleep(100 * t.Millisecond)

	var user string = c.Params.Get("username")
	var pass string = c.Params.Get("password")

	if len(user) == 0 || len(pass) == 0 {
		c.Response.Status = http.StatusBadRequest
		return nil
	}

	revel.INFO.Printf("user %s logging in", user)
	if pass[0:1] == "9" {
		c.Response.Status = http.StatusConflict
		return nil
	}
	if isValid(user, pass) {
		revel.INFO.Printf("login successful")
		c.Response.Status = http.StatusOK

		// use last digit of dni as user id (mod 4).
		id := last(user)
		user, present := m.FindUser(id)
		if present {
			return c.RenderJson(user)
		} else {
			revel.WARN.Printf("user not found")
			c.Response.Status = http.StatusNotFound
			return nil
		}
	}

	revel.WARN.Printf("login failed")
	c.Response.Status = http.StatusUnauthorized
	return nil
}

func (c Access) ChangePassword() revel.Result {
	// simulate lantency.
	t.Sleep(100 * t.Millisecond)

	var user string = c.Params.Get("username")
	var newpass string = c.Params.Get("newpasswd")

	if len(user) == 0 || len(newpass) == 0 {
		c.Response.Status = http.StatusBadRequest
		return nil
	}

	revel.INFO.Printf("changing password for user %s", user)
	if isValidPassword(newpass) {
		revel.INFO.Printf("change successful")
		c.Response.Status = http.StatusOK
		return nil
	}
	revel.WARN.Printf("change failed")
	c.Response.Status = http.StatusUnauthorized
	return nil
}
