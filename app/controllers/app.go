package controllers

import "github.com/revel/revel"

type LagoneApp struct {
	*revel.Controller
}

func (c LagoneApp) HealthCheck() revel.Result {
	return c.RenderJson("ok")
}
