package controllers

import "github.com/revel/revel"
import m "bitbucket.org/fernandezpablo85/lagone/app/models"

type Patients struct {
	LagoneApp
}

func (c Patients) Get(id int, udn int) revel.Result {
	records := m.FindById(id, udn)
	if len(records) > 0 {
		return c.RenderJson(records)
	} else {
		c.Response.Status = 404
		return c.RenderJson("")
	}
}

func (c Patients) Census(udn int) revel.Result {
	if udn > 0 {
		all := m.Census(udn)
		return c.RenderJson(all)
	} else {
		c.Response.Status = 400
		return nil
	}
}

func (c Patients) MonsterCall(udn int, id int, admision int, apellido string, nombre string, documento string, tipoDocumento string) revel.Result {

	if id > 0 {
		records := m.FindById(id, udn)
		if len(records) == 0 {
			c.Response.Status = 404
			return nil
		}
		return c.RenderJson(records)
	}

	if admision > 0 {
		userByAdmission, found := m.FindByAdmission(admision)
		if !found {
			c.Response.Status = 404
			return nil
		}
		return c.RenderJson(userByAdmission)
	}

	if len(apellido) > 0 {
		var results []m.Patient
		if len(apellido) <= 4 {
			results = []m.Patient{}
		} else {
			results = m.FindByName(apellido, nombre, udn)
		}
		return c.RenderJson(results)
	}

	if len(documento) > 0 && len(tipoDocumento) > 0 {
		var results []m.Patient
		if len(documento) <= 4 {
			results = []m.Patient{}
		} else {
			results = m.FindByPersonalId(documento, tipoDocumento, udn)
		}
		return c.RenderJson(results)
	}

	c.Response.Status = 400
	return nil
}

func (c Patients) DailyQueue(idProfesional int, idCentro int) revel.Result {
	if idCentro > 0 {
		all := m.DailyQueue(idCentro, idProfesional)
		return c.RenderJson(all)
	} else {
		c.Response.Status = 400
		return nil
	}
}
