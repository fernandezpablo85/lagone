package models

import "strings"
import "strconv"
import "log"

/*
  sample:

{
    "apellido": "pablo",
    "centro": {
        "codigo": "TRC",
        "id": 3,
        "nombre": "Trinidad Palermo",
        "sanatorio": {
            "codigo": "TRI",
            "id": 12,
            "nombre": "Trinidad Palermo"
        }
    },
    "especialidades": [
        {
            "codigo": "CLI",
            "id": 1,
            "nombre": "Clinica Medica"
        }
    ],
    "id": 1429047471318,
    "nombre": "pablo",
    "rol": {
        "descripcion": "Medico UCO",
        "id": 2
    },
    "sectores": [
        {
            "descripcion": "UTI",
            "id": 1
        },
        {
            "descripcion": "UCO",
            "id": 2
        }
    ]
}

}

*/
type Area struct {
	Id          int    `json:"id"`
	Description string `json:"descripcion, omitempty"`
}

type IdType struct {
	Acron string `json:"sigla"`
}

type Role struct {
	Id          int    `json:"id"`
	Description string `json:"codigo, omitempty"`
	Code        string `json:"descripcion, omitempty"`
}

type Clinic struct {
	Id   int    `json:"id"`
	Code string `json:"codigo"`
	Name string `json:"nombre"`
}

type Center struct {
	Id     int    `json:"id"`
	Code   string `json:"codigo"`
	Name   string `json:"nombre"`
	Clinic Clinic `json:"udnDto"`
}

type Department struct {
	Id       int    `json:"id"`
	Code     string `json:"codigo"`
	Name     string `json:"descripcion"`
	LongName string `json:"descripcionLarga"`
}

type Info struct {
	Id             int          `json:"id"`
	FirstName      string       `json:"nombre, omitempty"`
	LastName       string       `json:"apellido, omitempty"`
	Centers        []Center     `json:"centros"`
	Departments    []Department `json:"subEspecialidades"`
	Roles          []Role       `json:"roles, omitempty"`
	Areas          []Area       `json:"sectores"`
	PersonalId     string       `json:"numeroDocumento, omitempty"`
	PersonalIdType IdType       `json:"tipoDocumento, omitempty"`
}

var palermo = Center{3, "TRC", "Trinidad Palermo", Clinic{3, "INT", "Trinidad Palermo"}}
var quilmes = Center{2, "QLM", "Trinidad Quilmes", Clinic{2, "INT", "Trinidad Quilmes"}}
var ramos = Center{4, "RAM", "Trinidad Ramos Ambulatorio", Clinic{4, "AMB", "Trinidad Ramos Ambulatorio"}}
var centro1 = []Center{palermo}
var centro2 = []Center{palermo, quilmes}
var centro3 = []Center{ramos, quilmes}

var uti = Area{1, "UTI"}
var uco = Area{2, "UCO"}
var inte = Area{3, "INT"}

var idt = IdType{"DNI"}

var clinica = Department{1, "CLI", "Clinica Medica", "Clinica Medica"}
var cirugia = Department{2, "CIR", "Cirugia", "Clinica Medica"}
var especialidad1 = []Department{clinica}
var especialidad2 = []Department{clinica, cirugia}

var lectura = Role{0, "SoloLectura", "0"}
var escritura = Role{1, "Escribir", "1"}
var impresion = Role{2, "Imprimir", "2"}
var contingencia = Role{3, "RECONCILIAR_CONTINGENCIA", "3"}
var ambulatorio = Role{3, "Atiende Ambulatorio", "4"}

var all = map[int]Info{
	1: Info{1, "pablo", "fernandez", centro2, especialidad1, []Role{impresion}, []Area{uti}, "1111", idt},
	2: Info{2, "mauro", "garcia aurelio", centro2, especialidad1, []Role{escritura, impresion, ambulatorio, contingencia}, []Area{uti}, "1111", idt},
	3: Info{3, "alejandro", "bologna", centro2, especialidad2, []Role{escritura, impresion, ambulatorio, contingencia}, []Area{uti, uco}, "1111", idt},
	4: Info{4, "matias", "spanier", centro3, especialidad2, []Role{escritura, impresion, ambulatorio, contingencia}, []Area{inte}, "1111", idt},
}

func FindUser(_id string) (Info, bool) {
	id, err := strconv.Atoi(_id)
	user, ok := all[id]
	if err == nil && ok {
		return user, true
	} else {
		return Info{}, false
	}
}

func FindUsers(ids string) []Info {
	_ids := strings.Split(ids, ",")
	users := make([]Info, 0, 1)

	for _, id := range _ids {
		iid, err := strconv.Atoi(id)
		if err != nil {
			log.Printf("could not get a numeric value from '%s'", id)
			continue
		}
		u, found := all[iid]
		if found {
			users = append(users, u)
		}
	}
	return users
}
