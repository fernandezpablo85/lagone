package models

import (
	"testing"
)

var testCases = []struct {
	id       int
	name     string
	lastName string
}{
	{2027610, "PATRICIA CAMILA", "Bologna"},
	{2027480, "ROSARIO MARIA", "Garcia Aurelio"},
	{2027530, "JUAN JOSE", "Spanier"},
	{516131, "GASTON", "Fernandez"},
}

func TestFindPatient(t *testing.T) {
	for _, test := range testCases {
		records := FindById(test.id, 1)

		if len(records) == 0 {
			t.Fatalf("could not find patient with id %d", test.id)
		}

		if records[0].Info.Name != test.name {
			t.Fatalf("%v is not %s", test.name, records[0].Info.Name)
		}
		if records[0].Info.LastName != test.lastName {
			t.Fatalf("%v is not %s", test.lastName, records[0].Info.LastName)
		}
	}
}

func TestFindPatientsByArea(t *testing.T) {
	patientsInPalermo := Census(1)
	size := 16
	if len(patientsInPalermo) != size {
		t.Fatalf("wrong number of patients in udn = 1 (got %d instead of %d)", len(patientsInPalermo), size)
	}
}

func TestDailyQueue(t *testing.T) {
	appointments := DailyQueue(1, 1)
	size := 2
	if len(appointments) != size {
		t.Fatalf("wrong number of appointments in udn = 1 for professional = 1 (got %d instead of %d)", len(appointments), size)
	}
}
