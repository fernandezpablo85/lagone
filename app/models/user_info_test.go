package models

import (
	"testing"
)

func TestFindUsers(t *testing.T) {
	user := FindUsers("1")
	if len(user) != 1 {
		t.Fatalf("must return one user by id")
	}

	users := FindUsers("1,2,3,4")
	if len(users) != 4 {
		t.Fatalf("must return multiple users by ids separated with commas")
	}

	wrong := FindUsers("1,2,A,,3,4")
	if len(wrong) != 4 {
		t.Fatalf("must tolerate badly formatted string")
	}
}

func TestFindUser(t *testing.T) {
	user, found := FindUser("1")
	if !found || user.FirstName != "pablo" {
		t.Fatalf("expected name 'pablo' but got %s", user.FirstName)
	}
}
