package models

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type Patient struct {
	Id         int          `json:"id"`
	Admission  int          `json:"admision"`
	Entry      string       `json:"ingreso"`
	Info       PersonalInfo `json:"paciente"`
	Room       string       `json:"habitacion"`
	Bed        string       `json:"cama"`
	Tower      string       `json:"torre"`
	Sector     string       `json:"sector"`
	Floor      string       `json:"piso"`
	Udn        int          `json:"_udn"`
	Discharged string       `json:"fechaAlta, omitempty"`
}

type PersonalInfo struct {
	LastName       string `json:"apellido"`
	MotherLastName string `"json:"apellidoCasada, omitempty"`
	Name           string `json:"nombre"`
	Birthday       string `json:"fechaNacimiento"`
	Document       string `json:"documento"`
	Gender         string `json:"sexoDto"`
	DocumentType   string `json:"tipoDocumento"`
	Id             int    `json:"id"`
}

type Appointment struct {
	Id                string `json:"turnoId"`
	PacienteId        int    `json:"pacienteId"`
	PersonalIdType    string `json:"pacienteTipoDoc"`
	PersonalId        string `json:"pacienteNroDoc"`
	LastName          string `json:"pacienteApellido"`
	FirstName         string `json:"pacienteNombre"`
	Gender            string `json:"pacienteSexo"`
	HCProvider        string `json:"financiador"`
	HCPlan            string `json:"plan"`
	HCMemberId        string `json:"nroAfiliado"`
	AppointmentType   string `json:"tipoTurno"`
	AppointmentDate   string `json:"fechaTurno"`
	AppointmentTime   string `json:"horaTurno"`
	AppointmentStatus string `json:"estado"`
}

var allPatients []Patient
var allAppointments []Appointment

func init() {
	path := os.Getenv("GOPATH") + "/src/bitbucket.org/fernandezpablo85/lagone/conf/patients.json"
	file, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal("could not read json file", err)
	}
	err = json.Unmarshal(file, &allPatients)
	if err != nil {
		log.Fatal("could not unmarshall json", err)
	}
	path = os.Getenv("GOPATH") + "/src/bitbucket.org/fernandezpablo85/lagone/conf/appointments.json"
	file, err = ioutil.ReadFile(path)
	if err != nil {
		log.Fatal("could not read json file", err)
	}
	err = json.Unmarshal(file, &allAppointments)
	if err != nil {
		log.Fatal("could not unmarshall json", err)
	}
}

func FindByAdmission(admission int) (Patient, bool) {
	for _, patient := range allPatients {
		if patient.Admission == admission {
			return patient, true
		}
	}
	return Patient{}, false
}

func FindById(id int, udn int) []Patient {
	all := make([]Patient, 0, 1)
	for _, patient := range allPatients {
		// patient.Udn == udn &&
		if patient.Id == id {
			all = append(all, patient)
		}
	}
	return all
}

func FindByPersonalId(documento string, tipoDocumento string, udn int) []Patient {
	all := make([]Patient, 0, 1)
	for _, p := range allPatients {
		if p.Udn == udn && p.Info.Document == documento && p.Info.DocumentType == tipoDocumento {
			all = append(all, p)
		}
	}
	return all
}

func FindByName(apellido string, nombre string, udn int) []Patient {
	all := make([]Patient, 0, 1)
	for _, p := range allPatients {
		if p.Udn == udn && strings.Trim(p.Info.LastName, " ") == apellido {
			all = append(all, p)
		}
	}
	return all
}

func Census(udn int) []Patient {
	all := make([]Patient, 0, 1)
	for _, patient := range allPatients {
		// if patient.Udn == udn && len(patient.Discharged) == 0 {
		all = append(all, patient)
		// }
	}
	return all
}

func DailyQueue(udn int, professionalId int) []Appointment {
	all := make([]Appointment, 0, 1)
	for _, appt := range allAppointments {
		all = append(all, appt)
	}
	return all
}
